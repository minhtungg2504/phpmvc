<?php
include_once("e_sinhvien.php");
class M_SinhVien {
    public function __construct() {}
    public function getAllSinhVien() {
        //Khai báo kết nối
        $link = mysqli_connect('localhost', 'root', '') or die('Could not connect to MySQL Database');
        mysqli_select_db($link, "test1");
        $sql = "Select * from sinhviens";
        $result = mysqli_query($link, $sql);
        $res_arr = array();
        //In tiêu đề của bảng
        $i = 0;
        while ($row = mysqli_fetch_array($result)) {
            $i++;
            $res_arr[$i] = new E_SinhVien($row['mssv'], $row['hoten'], $row['gioitinh'], $row['khoa']);
            // array_push($res_arr, $i => new E_User($row['id'], $row['firstname'], $row['lastname'], $row['username'], $row['password'], $row['role']));
        }
        //Giải phóng tập các bản ghi trong $result
        mysqli_free_result($result);
        mysqli_close($link);
        return $res_arr;
    }
    public function searchKhoaSinhVien($search) {
        //Khai báo kết nối
        $link = mysqli_connect('localhost', 'root', '') or die('Could not connect to MySQL Database');
        mysqli_select_db($link, "test1");
        $sql = "Select * from sinhviens WHERE khoa='".$search."'";
        $result = mysqli_query($link, $sql);
        $res_arr = array();
        //In tiêu đề của bảng
        $i = 0;
        while ($row = mysqli_fetch_array($result)) {
            $i++;
            $res_arr[$i] = new E_SinhVien($row['mssv'], $row['hoten'], $row['gioitinh'], $row['khoa']);
            // array_push($res_arr, $i => new E_User($row['id'], $row['firstname'], $row['lastname'], $row['username'], $row['password'], $row['role']));
        }
        //Giải phóng tập các bản ghi trong $result
        mysqli_free_result($result);
        mysqli_close($link);
        return $res_arr;
    }
    public function themSinhVien($mssv, $hoten, $gioitinh, $khoa) {
        //Khai báo kết nối
        $link = mysqli_connect('localhost', 'root', '') or die('Could not connect to MySQL Database');
        mysqli_select_db($link, "test1");
        $sql = "INSERT INTO sinhviens(mssv, hoten, gioitinh, khoa) VALUES('{$mssv}', '{$hoten}', '{$gioitinh}', '{$khoa}')";
        // echo $sql;
        $result = mysqli_query($link, $sql);
        //Giải phóng tập các bản ghi trong $result
        // mysqli_free_result($result);
        echo $result;
        mysqli_close($link);
    }
    public function getSinhVien($mssv) {
        //Khai báo kết nối
        $link = mysqli_connect('localhost', 'root', '') or die('Could not connect to MySQL Database');
        mysqli_select_db($link, "test1");
        $sql = "Select * from sinhviens WHERE mssv='".$mssv."'";
        // echo $sql;
        $result = mysqli_query($link, $sql);
        //Giải phóng tập các bản ghi trong $result
        // mysqli_free_result($result);
        while ($row = mysqli_fetch_array($result)) {
            $res = new E_SinhVien($row['mssv'], $row['hoten'], $row['gioitinh'], $row['khoa']);
            // array_push($res_arr, $i => new E_User($row['id'], $row['firstname'], $row['lastname'], $row['username'], $row['password'], $row['role']));
        }
        mysqli_free_result($result);
        mysqli_close($link);
        return $res;
    }
    public function xoaSinhVien($mssv) {
        //Khai báo kết nối
        $link = mysqli_connect('localhost', 'root', '') or die('Could not connect to MySQL Database');
        mysqli_select_db($link, "test1");
        $sql = "DELETE FROM sinhviens WHERE mssv='".$mssv."'";
        // echo $sql;
        $result = mysqli_query($link, $sql);
        mysqli_close($link);
    }
    public function capnhatSinhVien($mssv, $hoten, $gioitinh, $khoa) {
        //Khai báo kết nối
        $link = mysqli_connect('localhost', 'root', '') or die('Could not connect to MySQL Database');
        mysqli_select_db($link, "test1");
        $sql = "UPDATE sinhviens SET hoten='{$hoten}', gioitinh='{$gioitinh}', khoa='{$khoa}' WHERE mssv='{$mssv}'";
        // echo $sql;
        $result = mysqli_query($link, $sql);
        mysqli_close($link);
    }
}
?>